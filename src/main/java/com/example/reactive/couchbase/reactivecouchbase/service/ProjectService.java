package com.example.reactive.couchbase.reactivecouchbase.service;

import com.example.reactive.couchbase.reactivecouchbase.entity.Project;
import com.example.reactive.couchbase.reactivecouchbase.entity.Task;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProjectService {

	Mono<Project> createProject(Project project);

	Mono<Task> createTask(Task task);

	Flux<Project> findAll();

	Mono<Project> findProjectById(String projectId);

}
