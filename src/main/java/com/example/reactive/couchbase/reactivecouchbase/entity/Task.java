package com.example.reactive.couchbase.reactivecouchbase.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;
import org.springframework.data.couchbase.repository.Collection;
import org.springframework.data.couchbase.repository.Scope;

import lombok.Data;

@Document
@Data
@Scope(value = "dev")
@Collection(value = "Task")
public class Task {
	@Id
	private String _id;
	@Field("pid")
	private String projectId;
	private String name;
	@Field("desc")
	private String description;
	private String ownerName;
	private Long cost;
	@Version
	private Long version;

}
