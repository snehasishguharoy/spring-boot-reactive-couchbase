package com.example.reactive.couchbase.reactivecouchbase.cnfg;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

@Configuration
@EnableCouchbaseRepositories(basePackages = "com.example.reactive.couchbase.reactivecouchbase.repo")
public class CouchBaseCnfg extends AbstractCouchbaseConfiguration {

	@Override
	public String getConnectionString() {
		return "couchbase://localhost";
	}

	@Override
	public String getUserName() {
		return "admin";
	}

	@Override
	public String getPassword() {
		return "password";
	}

	@Override
	public String getBucketName() {
		return "snehasish";
	}

}
