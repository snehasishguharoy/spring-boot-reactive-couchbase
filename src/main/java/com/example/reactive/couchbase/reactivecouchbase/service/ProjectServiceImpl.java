package com.example.reactive.couchbase.reactivecouchbase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.reactive.couchbase.reactivecouchbase.entity.Project;
import com.example.reactive.couchbase.reactivecouchbase.entity.Task;
import com.example.reactive.couchbase.reactivecouchbase.repo.ProjectRepo;
import com.example.reactive.couchbase.reactivecouchbase.repo.TaskRepo;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectRepo projectRepo;
	@Autowired
	private TaskRepo taskRepo;

	@Override
	public Mono<Project> createProject(Project project) {
		return projectRepo.save(project);
	}

	@Override
	public Mono<Task> createTask(Task task) {
		return taskRepo.save(task);
	}

	@Override
	public Flux<Project> findAll() {
		return projectRepo.findAll();
	}

	@Override
	public Mono<Project> findProjectById(String projectId) {
		return projectRepo.findById(projectId);
	}

}
