package com.example.reactive.couchbase.reactivecouchbase.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.reactive.couchbase.reactivecouchbase.entity.Project;
import com.example.reactive.couchbase.reactivecouchbase.entity.Task;
import com.example.reactive.couchbase.reactivecouchbase.service.ProjectService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/project")
public class ProjectController {
	@Autowired
	private ProjectService projectService;

	@PostMapping("/save")
	public Mono<ResponseEntity<Project>> createProject(@RequestBody Project project) {
		return projectService.createProject(project).map(data -> ResponseEntity.created(null).body(data));
	}

	@PostMapping("/save/task")
	public Mono<ResponseEntity<Task>> createTask(@RequestBody Task task) {
		return projectService.createTask(task).map(data -> ResponseEntity.created(null).body(data));
	}

	@GetMapping("/find/all")
	public Mono<ResponseEntity<List<Project>>> findAll() {
		return projectService.findAll().collectList().map(data -> ResponseEntity.created(null).body(data));
	}

	@GetMapping("/find/{id}")
	public Mono<ResponseEntity<Project>> findById(@PathVariable("id") String id) {
		return projectService.findProjectById(id).map(data -> ResponseEntity.created(null).body(data));
	}

}
