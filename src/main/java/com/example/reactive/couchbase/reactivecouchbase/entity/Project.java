package com.example.reactive.couchbase.reactivecouchbase.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;
import org.springframework.data.couchbase.repository.Collection;
import org.springframework.data.couchbase.repository.Scope;


import lombok.Data;
@Document
@Data
@Scope(value = "dev")
@Collection(value = "Project")
public class Project {
	@Id
	private String _id;
	private String name;
	private String code;
	@Field("desc")
	private String description;
	private String startDate;
	private String endDate;
	@Field("cost")
	private Long estinmatedCost;
	private List<String> countryList;
	@Version
	private Long version;
}
