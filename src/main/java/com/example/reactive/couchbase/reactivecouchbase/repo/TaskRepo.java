package com.example.reactive.couchbase.reactivecouchbase.repo;

import org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository;

import com.example.reactive.couchbase.reactivecouchbase.entity.Task;

public interface TaskRepo extends ReactiveCouchbaseRepository<Task, String> {

}
