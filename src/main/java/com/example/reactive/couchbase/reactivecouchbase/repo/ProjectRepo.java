package com.example.reactive.couchbase.reactivecouchbase.repo;

import org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository;

import com.example.reactive.couchbase.reactivecouchbase.entity.Project;

public interface ProjectRepo extends ReactiveCouchbaseRepository<Project, String> {

}
